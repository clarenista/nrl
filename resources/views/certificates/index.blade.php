<html>
    <head>
        <style>
            body, html {
                margin: 0;
                padding: 0;
            }
            body {
                color: black;
                display: table;
                font-family: Georgia, serif;
                font-size: 24px;
                text-align: center;
            }
            .container {
                border: 1px solid black;
                width: 750px;
                height: 563px;
                display: table-cell;
                vertical-align: middle;
            }
            .logo {
                color: tan;
            }

            .marquee {
                color: tan;
                font-size: 48px;
                margin: 20px;
            }
            .assignment {
                margin: 20px;
            }
            .person {
                border-bottom: 2px solid black;
                font-size: 32px;
                font-style: italic;
                margin: 20px auto;
                width: 400px;
            }
            .reason {
                margin: 20px;
            }
        </style>
    </head>
    <body>
    <table style="font-family: Times New Roman;">
                <tr style="text-align:center; ">
                    <td style="width:30%">    
                        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6f/DOH_Logo.png" width="100"/>
                    </td>
                    <td style="width:40%">    
                        <h2>Republic of the Philippines Department of Health <br>Manila </h2>
                    </td>
                    <td style="width:30%">    
                        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6f/DOH_Logo.png" width="100"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr style="text-align:center; ">
                    <td colspan="3">
                        <p>this</p>
                    </td>
                </tr>
                <tr style="text-align:center; ">
                    <td colspan="3">
                        <h1>CERTIFICATE OF PROFICIENCY</h1>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr style="text-align:center;" >
                    <td colspan="3">
                        <p>is hereby presented to</p>
                        <h2>QUEEN CHECK DRUG TESTING LABORATORY</h2>
                        <p style="padding:0px; margin:0px;">2ND FLOOR NEW PUBLIC MARKET, ROXAS BLVD., SAN CARLOS CITY, PANGASINAN</p>
                        <p>Certificate No. SDTLPT-18-011306</p>
                        <p>Accreditation No. 01-1275-18-PFSS-R</p>
                        <br>
                        <p>for successful participation, passing and achieving</p>
                        <h2>HIGHLY SATISFACTORY</h2>
                        <p>performance in the</p>
                        <h2>“CY 2018 PROFICIENCY TESTING SCHEME for SCREENING DRUGS OF ABUSE TESTING” </h2>
                        <br>
                        <p>Given this 30th day of April 2019</p>
                    </td>
                </tr>
                <tr style="text-align:center;">
                    <td style="width:45%">
                        <p>JENNIFER D. MERCADO, MD, MMHoA, FPSP</p>
                        <p>Head, National Reference Laboratory</p>
                        <p>East Avenue Medical Center 
                            <span><img src="https://upload.wikimedia.org/wikipedia/commons/6/6f/DOH_Logo.png" width="10"></span>
                        </p>
                        </td>
                    <td style="width:10%"></td>
                    <td style="width:45%">
                        <p>ATTY. NICOLAS B. LUTERO III, CESO III</p>
                        <p>Director IV</p>
                        <p>Health Facilities and Services Regulatory Bureau</p>
                    </td>
                </tr>
            </table>
    </body>
</html>